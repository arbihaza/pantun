from flask import Flask, render_template, redirect, url_for, request, flash
import pymysql.cursors, os
from lib import *

app = Flask(__name__)
app.secret_key='$!$4$!$4'

conn = cursor = None
def openDb():
    global conn, cursor
    conn = pymysql.connect(
        host='localhost',
        user='root',
        password='',
        database='pantun',
        cursorclass=pymysql.cursors.DictCursor)
    cursor = conn.cursor()	

def closeDb():
    global conn, cursor
    cursor.close()
    conn.close()

def searchPantun(isi1,isi2):
    openDb()
    sql = "SELECT sampiran1, sampiran2 FROM pantun GROUP BY sampiran1,sampiran2 ORDER BY id ASC"
    cursor.execute(sql)
    results = cursor.fetchall()
    # conn.commit()
    closeDb()

    max_score = 0
    best_sampiran1 = "tidak ada sampiran sesuai"
    best_sampiran2 = "tidak ada sampiran sesuai"
    for result in results:
        #if result['sampiran1'][-1]==isi1[-1] and result['sampiran2'][-1]==isi2[-1]:
        current_score = matchpantun(result['sampiran1'], result['sampiran2'], isi1, isi2)
        if current_score > max_score:
            max_score = current_score
            best_sampiran1 = result['sampiran1']
            best_sampiran2 = result['sampiran2']
    return (best_sampiran1, best_sampiran2, max_score)

@app.route('/')
def index():
    return render_template('index.html',
        title="Home"
    )

@app.route('/buat')
def buat():
    return render_template('index.html',
        title="Buat"
    )

@app.route('/kontribusi')
def kontribusi():
    return render_template('index.html',
        title="Kontribusi"
    )

@app.route("/hasil", methods=['POST'])
def hasil():
    if request.method == 'POST':
        isi1 = request.form['isi1']
        isi2 = request.form['isi2']
        sampiran1, sampiran2, score = searchPantun(isi1.lower(), isi2.lower())
        flash(f'Rekomendasi Sampiran berhasil dibuat dengan kemiripan {score} huruf','success')
        return render_template('result.html',
            title="Hasil Pantun",
            sampiran1=sampiran1.title(),
            sampiran2=sampiran2.title(),
            isi1=isi1.title(),
            isi2=isi2.title()
        )
    else:
        return redirect(url_for('index'))
    
@app.route("/kontribusi_proc", methods=['POST'])
def kontribusi_proc():
    if request.method == 'POST':
        sampiran1 = request.form['sampiran1']
        sampiran2 = request.form['sampiran2']
        isi1 = request.form['isi1']
        isi2 = request.form['isi2']
        openDb()
        sql = "INSERT INTO pantun(sampiran1,sampiran2,isi1,isi2) VALUES (%s,%s,%s,%s)"
        val = (sampiran1.lower(),sampiran2.lower(),isi1.lower(),isi2.lower())
        try:
            cursor.execute(sql, val)
            conn.commit()
            flash('Berhasil Menambahkan Pantun !','success')
            return redirect(url_for('kontribusi'))
        except:
            flash('Gagal Menambahkan Pantun !','danger')
            return redirect(url_for('kontribusi'))
        finally:
            closeDb()
    else:
        return redirect(url_for('index'))

if __name__ == "__main__":
    app.run(debug=True)
