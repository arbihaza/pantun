def tailsimilarity(word1, word2, tail1, tail2, bobot):
    if word1 == word2:
        return len(word1)
    if len(word1) == 0 or len(word2) == 0:
        return bobot
    if word1[-1] == word2[-1]:
        return tailsimilarity(word1[:-1], word2[:-1], word1[-1], word2[-1], bobot+1)
    else:
        if tail1 in ['a','i','u','e','o']:
            return bobot
        else:
            return 0
    #return tailsimilarity(word1[:-1], word2[:-1], word1[-1], word2[-1], bobot+1)

def matchpantun(sampiran1, sampiran2, isi1, isi2):
    bobotA = 0
    bobotB = 0
    k_sampiran1 = sampiran1.split(' ')[-1]
    k_sampiran2 = sampiran2.split(' ')[-1]
    k_isi1 = isi1.split(' ')[-1]
    k_isi2 = isi2.split(' ')[-1]

    bobotA = tailsimilarity(k_sampiran1, k_isi1, '#', '#', 0)
    bobotB = tailsimilarity(k_sampiran2, k_isi2, '#', '#', 0)
    
    if bobotA == 0  or bobotB == 0:
        totalBobot = 0
    else:
        totalBobot = bobotA + bobotB
    
    return totalBobot
